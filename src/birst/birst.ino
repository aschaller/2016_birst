// This is where everything comes together.


#include <Wire.h>
#include "SparkFunMPL3115A2.h"
#include "SparkFunHTU21D.h"
#include <SPI.h>
#include <SD.h>

File fd;
File weatherFile;
File imuFile;
const uint8_t BUFFER_SIZE = 100;
char fileName[] = "WData.txt"; // SD library only supports up to 8.3 names
char fileName2[] = "IMUData.txt";
char buff[BUFFER_SIZE+2] = "";  // Added two to allow a 2 char peek for EOF state
uint8_t index = 0;

const uint8_t chipSelect = 8;
const uint8_t cardDetect = 22;

enum states: uint8_t { NORMAL, E, EO };
uint8_t state = NORMAL;

bool alreadyBegan = false;  // SD.begin() misbehaves if not first call

MPL3115A2 myPressure;
HTU21D myHumidity;


// Digital I/O pins
const byte STAT1 = 7; //blue LED
const byte STAT2 = 8; //red LED

//Analog I/O pins
const byte power = A3;
const byte LIGHT = A1;
const byte BATT = A2;

//Time Variables
long lastSecond; //The millis counter to see when a second rolls by
byte seconds; //When it hits 60, increase the current minute
byte seconds_2m; //Keeps track of the "wind speed/dir avg" over last 2 minutes array of data
byte minutes; //Keeps track of where we are in various arrays of data
byte minutes_10m; //Keeps track of where we are in wind gust/dir over last 10 minutes array of data

float pressure = 0;
float humidity = 0;
float temp = 0;
float rainin = 0;
float altitude = 0;
int count = 1;
int count2 = 1;

float batt_lvl = 11.8;
float light_lvl = 455;
String buffer;
String inByte;
int inByte2;

String val = "";
String val2 = "";
char weatherBuff [200];
char buff2[200];



void initializeCard(void)
{
  Serial.print(F("Initializing SD card..."));

  // Is there even a card?
  if (!digitalRead(cardDetect))
  {
    Serial.println(F("No card detected. Waiting for card."));
    while (!digitalRead(cardDetect));
    delay(250); // 'Debounce insertion'
  }

  // Card seems to exist.  begin() returns failure
  // even if it worked if it's not the first call.
  if (!SD.begin() && !alreadyBegan)  // begin uses half-speed...
  {
    Serial.println(F("Initialization failed!"));
    initializeCard(); // Possible infinite retry loop is as valid as anything
  }
  else
  {
    alreadyBegan = true;
  }
  Serial.println(F("Initialization done."));

  Serial.print(fileName);
  if (SD.exists(fileName))
  {
    Serial.println(F(" exists."));
  }
  else
  {
    Serial.println(F(" doesn't exist. Creating."));
  }

  Serial.print("Opening file: ");
  Serial.println(fileName);

  Serial.println(F("Enter text to be written to file. 'EOF' will terminate writing."));
}


////////////////////////////////////////////////////////////////////////////////
// This function is called after the EOF command is received. It writes the
// remaining unwritten data to the µSD card, and prints out the full contents
// of the log file.
////////////////////////////////////////////////////////////////////////////////
void eof(void)
{
  index -= 3; // Remove EOF from the end
  flushBuffer();

  // Re-open the file for reading:
  fd = SD.open(fileName);
  if (fd)
  {
    Serial.println("");
    Serial.print(fileName);
    Serial.println(":");

    while (fd.available())
    {
      Serial.write(fd.read());
    }
  }
  else
  {
    Serial.print("Error opening ");
    Serial.println(fileName);
  }
  fd.close();
}


////////////////////////////////////////////////////////////////////////////////
// Write the buffer to the log file. If we are possibly in the EOF state, verify
// that to make sure the command isn't written to the file.
////////////////////////////////////////////////////////////////////////////////
void flushBuffer(void)
{
  fd = SD.open(fileName, FILE_WRITE);
  if (fd) {
    switch (state)  // If a flush occurs in the 'E' or the 'EO' state, read more to detect EOF
    {
    case NORMAL:
      break;
    case E:
      readByte();
      readByte();
      break;
    case EO:
      readByte();
      break;
    }
    fd.write(buff, index);
    fd.flush();
    index = 0;
    fd.close();
  }
}



////////////////////////////////////////////////////////////////////////////////
// Reads a byte from the serial connection. This also maintains the state to
// capture the EOF command.
////////////////////////////////////////////////////////////////////////////////
void readByte(void)
{
  byte byteRead = Serial.read();
  Serial.write(byteRead); // Echo
  buff[index++] = byteRead;

  // Must be 'EOF' to not get confused with words such as 'takeoff' or 'writeoff'
  if (byteRead == 'E' && state == NORMAL)
  {
    state = E;
  }
  else if (byteRead == 'O' && state == E)
  {
    state = EO;
  }
  else if (byteRead == 'F' && state == EO)
  {
    eof();
    state = NORMAL;
  }
}

void setup () {
  Serial.begin(9600);
  Serial3.begin(57600);
//  while (!Serial && !Serial3); //waits til all ports are connected
  
  Serial3.write("1");
  //Serial.println("Beginning data collection");

  pinMode(STAT1, OUTPUT); //blue LED Status
  pinMode(STAT2,OUTPUT); //red LED Status

  pinMode(power, INPUT);
  pinMode(LIGHT, INPUT);
  
  pinMode(53,OUTPUT);
  pinMode(cardDetect, INPUT);

  initializeCard();

    //Configure the pressure sensor
  myPressure.begin(); // Get sensor online
  myPressure.setModeBarometer(); // Measure pressure in Pascals from 20 to 110 kPa
  myPressure.setOversampleRate(7); // Set Oversample to the recommended 128
  myPressure.enableEventFlags(); // Enable all three pressure and temp event flags

  //Configure the humidity sensor
  myHumidity.begin();

  seconds = 0;
  lastSecond = millis();

  weatherFile = SD.open(fileName,FILE_WRITE);
  if (weatherFile) {
    weatherFile.println("            Weather Shield Data Collected               ");
    weatherFile.flush();
    weatherFile.close();
  } else {
    Serial.println("Setup method: Error opening Weather Shield Data Txt file");
  }

  
  
  imuFile = SD.open(fileName2,FILE_WRITE);
  if (imuFile) {
    imuFile.println("            IMU Data Collected               ");
    imuFile.flush();
    imuFile.close();
  }  else {
    Serial.println("Setup Method: Error opening IMU Data Txt file");
  }
  
  
}

float get_battery_lvl(){
  float operatingVoltage = analogRead(power);

  float rawVoltage = analogRead(BATT);

  operatingVoltage = 3.30 / operatingVoltage; //The reference voltage is 3.3V

  rawVoltage = operatingVoltage * rawVoltage; //Convert the 0 to 1023 int to actual voltage on BATT pin

  rawVoltage *= 4.90; //(3.9k+1k)/1k - multiple BATT voltage by the voltage divider to get actual system voltage

  return(rawVoltage);
}

float get_light_lvl(){
  float operatingVoltage = analogRead(power);

  float lightSensor = analogRead(LIGHT);

  operatingVoltage = 3.3 / operatingVoltage; //The reference voltage is 3.3V

  lightSensor = operatingVoltage * lightSensor;

  return(lightSensor);
}

String inData;
void loop () {
 // Serial.println(millis());
  //Serial.print(lastSecond);
  
 // if(millis() - lastSecond >= 1000) {
    lastSecond += 1000;
   // Serial.print("hello");
    digitalWrite(STAT1, HIGH);//turn on blue LED.
    lastSecond += 1000;
    humidity = myHumidity.readHumidity();
    pressure = myPressure.readPressure();
    altitude = myPressure.readAltitudeFt();

    temp = myPressure.readTempF();   
     
    Serial.println(temp);
    batt_lvl = get_battery_lvl();
    light_lvl = get_light_lvl();

    // send through transcievers

  delay(1000);
  //}

   if (!digitalRead(cardDetect)) {
    initializeCard();
   }

   weatherFile = SD.open(fileName, FILE_WRITE);

   if (weatherFile) {
   
    Serial.println(humidity);
    sprintf(weatherBuff,"%s%i%s%f","humidity", count, ": ", humidity);
    weatherFile.write(weatherBuff,sizeof(weatherBuff));
    weatherFile.println(weatherBuff);
    weatherFile.write(weatherBuff);
    weatherFile.flush();
    sprintf(weatherBuff,"%s%i%s%f","Pressure", count, ": ", pressure);
    weatherFile.println(weatherBuff);
    sprintf(weatherBuff,"%s%i%s%f","Altitude(ft)", count, ": ", altitude);
    weatherFile.println(weatherBuff);
    sprintf(weatherBuff,"%s%i%s%f","Temperature", count, ": ", temp);
    weatherFile.println(weatherBuff);
    sprintf(weatherBuff,"%s%i%s%f", "Battery Level", count, ": ", batt_lvl);
    weatherFile.println(weatherBuff);
    sprintf(weatherBuff,"%s%i%s%f", "Light Level", count, ": ", light_lvl);
    weatherFile.println(weatherBuff);
    ++count;
    weatherFile.close();
   } else {
    Serial.println("Loop Method: Error opening Weather Shield Data Txt file");
   }

   
   
   val2 = getIMU_data(); // might have problems since it will always be collecting data.(Infinite loop?)
   sprintf(buff2,"%f%s", altitude, &val2);
   //buff needs to be sent through the transceiver.   
   
}



String getIMU_data() {
  
  Serial3.write("1");
  imuFile = SD.open("IMUData.txt",FILE_WRITE);
 
  while (Serial3.available() > 0 && count2 <= 2) {
    val = Serial3.readString();
    
    if (imuFile) {
      imuFile.println(val);
      imuFile.close();
    } else {
      Serial.println("error opening IMU Data Txt file");
    }
    count2++;
  // send to transceivers when it works. 
    
  }
  return val;
}




