/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
 */
 # include <Servo.h>
Servo m1;
int delayVal = 900;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin 13 as an output.
  pinMode(13, OUTPUT);
  m1.attach(6);
  delay(1);
  m1.write(10);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level) 
//  m1.write(10);
//  delay(delayVal);              // wait for a second
//  
  m1.write(60);
  delay(delayVal);
  
  m1.write(120);
  delay(delayVal);
  
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  m1.write(180);
  delay(delayVal);

  m1.write(120);
  delay(delayVal);

  m1.write(60);
  delay(delayVal);

}
