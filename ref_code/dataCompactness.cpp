#include <string>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>

// This currently doesnt work on windows. its a linux feature might work on arduino since its a linux bootloader on it.
// #include <zlib.h>

// std::string compress_string(const std::string& str,
//                             int compressionlevel = Z_BEST_COMPRESSION)
// {
//     z_stream zs;                        // z_stream is zlib's control structure
//     memset(&zs, 0, sizeof(zs));

//     if (deflateInit(&zs, compressionlevel) != Z_OK)
//         throw(std::runtime_error("deflateInit failed while compressing."));

//     zs.next_in = (Bytef*)str.data();
//     zs.avail_in = str.size();           // set the z_stream's input

//     int ret;
//     char outbuffer[32768];
//     std::string outstring;

//     // retrieve the compressed bytes blockwise
//     do {
//         zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
//         zs.avail_out = sizeof(outbuffer);

//         ret = deflate(&zs, Z_FINISH);

//         if (outstring.size() < zs.total_out) {
//             // append the block to the output string
//             outstring.append(outbuffer,
//                              zs.total_out - outstring.size());
//         }
//     } while (ret == Z_OK);

//     deflateEnd(&zs);

//     if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
//         std::ostringstream oss;
//         oss << "Exception during zlib compression: (" << ret << ") " << zs.msg;
//         throw(std::runtime_error(oss.str()));
//     }

//     return outstring;
// }

// /** Decompress an STL string using zlib and return the original data. */
// std::string decompress_string(const std::string& str)
// {
//     z_stream zs;                        // z_stream is zlib's control structure
//     memset(&zs, 0, sizeof(zs));

//     if (inflateInit(&zs) != Z_OK)
//         throw(std::runtime_error("inflateInit failed while decompressing."));

//     zs.next_in = (Bytef*)str.data();
//     zs.avail_in = str.size();

//     int ret;
//     char outbuffer[32768];
//     std::string outstring;

//     // get the decompressed bytes blockwise using repeated calls to inflate
//     do {
//         zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
//         zs.avail_out = sizeof(outbuffer);

//         ret = inflate(&zs, 0);

//         if (outstring.size() < zs.total_out) {
//             outstring.append(outbuffer,
//                              zs.total_out - outstring.size());
//         }

//     } while (ret == Z_OK);

//     inflateEnd(&zs);

//     if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
//         std::ostringstream oss;
//         oss << "Exception during zlib decompression: (" << ret << ") "
//             << zs.msg;
//         throw(std::runtime_error(oss.str()));
//     }

//     return outstring;
// }

long getTemp() {
	return 1;
}


long getAlt(){
	return 2;

}

long lightSensor(){
	return 3;
}
//this will collect the data and put it int a string. the compressing should work on arduino. test later
int main() {
	char buffer[56];
	
	snprintf(buffer, sizeof(buffer), "%d%d%d", getTemp(), getAlt(), lightSensor());
	//compress_string(buffer);  // will compress the string and return a std::string
	return 1;

}





